package rest.hi_api;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class HiAPITest {
    public static final String SERVER_ADDRESS = "http://materials.craftincode.com:8080";

    public static void main(String[] args) throws UnirestException {
        String simpleResopnse = Unirest.get(SERVER_ADDRESS + "/hi").asString().getBody();

        System.out.println(simpleResopnse);

        String paramRespone = Unirest.get(SERVER_ADDRESS + "/helloPathParam/Pawel")
                .asString().getBody();
        System.out.println(paramRespone);

        String requestParam = Unirest.get(SERVER_ADDRESS + "/helloRequestParam")
                .queryString("name", "666").asString().getBody();

        System.out.println(requestParam);

        String postParam = Unirest.post(SERVER_ADDRESS + "/helloPost")
                .header("Content-type", "application/json").body("hlava").asString().getBody();

        System.out.println(postParam);
    }
}
