package rest.adapter;

import java.util.Scanner;

public class Calculator {
    MathEngine engine;

    public Calculator(MathEngine engine) {
        this.engine = engine;
    }
    public void run() {
        System.out.println("Podaj liczbe a");
        double a = new Scanner(System.in).nextDouble();
        System.out.println("Podaj liczbe b");
        double b = new Scanner(System.in).nextDouble();
        double result = engine.add(a,b);
        System.out.println("Wynik dodawania to " + result);
    }
}
