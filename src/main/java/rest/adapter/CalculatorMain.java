package rest.adapter;

public class CalculatorMain {

    public static void main(String[] args) {
        Calculator calculator = new Calculator(new MathEngine() {
            PWRUberCalculator calculator = new PWRUberCalculator();
            @Override
            public double add(double a, double b) {
                return calculator.dodajLiczby(a,b);
            }

            @Override
            public double subst(double a, double b) {
                return calculator.odejmijLiczby(a,b);
            }

        });
    calculator.run();
    }
}
