package rest.adapter;

public interface MathEngine {
    double add(double a, double b);
    double subst(double a, double b);
}
