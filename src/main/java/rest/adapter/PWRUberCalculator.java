package rest.adapter;

public class PWRUberCalculator {
    public double dodajLiczby (double pierwsza, double druga){
        return pierwsza + druga;
    }

    public double odejmijLiczby (double pierwsza, double druga){
        return pierwsza - druga;
    }

    public double podzielLiczby (double pierwsza, double druga){
        return pierwsza / druga;
    }

    public double pomnozLiczby (double pierwsza, double druga){
        return pierwsza * druga;
    }

}
