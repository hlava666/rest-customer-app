package rest.customer_api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.IOException;

public class CustomerQuery {

    public static final String SERVER_ADDRESS = "http://materials.craftincode.com:8080";

    public static CustomerDTO[] getCustomerList() throws UnirestException, IOException {

        String customerJSON = Unirest.get(SERVER_ADDRESS + "/api/v1/customers").asString().getBody();
        ObjectMapper mapper = new ObjectMapper();
        CustomerDTO[] customerList = mapper.readValue(customerJSON,CustomerDTO[].class);
        return customerList;
    }

    public static void addCustomer(CustomerDTO customerDTO) throws UnirestException {
        Unirest.post(SERVER_ADDRESS + "/api/v1/customers")
                .header("Content-Type", "application/json")
                .body(customerDTO).asString().getBody();
    }

    public static void deleteCustomer(String customerId) throws UnirestException {
        Unirest.delete(SERVER_ADDRESS + "/api/v1/customers/" + customerId)
                .asString().getBody();
    }
}
