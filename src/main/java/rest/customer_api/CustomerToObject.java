package rest.customer_api;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.IOException;
import java.util.Scanner;
import java.util.UUID;

public class CustomerToObject {


    public static void main(String[] args) throws UnirestException, IOException {
        configureObjectMapper();
        boolean exit = false;
        int a;
        while (!exit) {
            System.out.println("=========== Simple REST application ===========");
            System.out.println("Lista mozliwosci:\n1- lista klientow,\n2- dodaj klienta,\n3- usun klienta,\n4- koniec");
            System.out.print("Twoj wybor: ");
            a = new Scanner(System.in).nextInt();
            switch (a) {
                case 1:
                    CustomerDTO[] list = CustomerQuery.getCustomerList();
                    System.out.println("=========== Lista klientow: ===========");
                    for (CustomerDTO c : list) {
                        System.out.println(c);
                    }
                    System.out.println("Kliknij ENTER, aby kontynuowac ...");
                    System.in.read();
                    break;
                case 2:
                    System.out.println("=========== Stworz nowego klienta: ===========");
                    CustomerDTO customerDTO = new CustomerDTO();
                    String tempString;
                    int tempInt;
                    double tempDouble;
                    customerDTO.setId(UUID.randomUUID().toString());
                    System.out.print("Podaj imie: ");
                    tempString = new Scanner(System.in).nextLine();
                    customerDTO.setFirstName(tempString);
                    System.out.print("Podaj nazwisko: ");
                    tempString = new Scanner(System.in).nextLine();
                    customerDTO.setLastName(tempString);
                    System.out.print("Podaj wzrost: ");
                    tempDouble = new Scanner(System.in).nextDouble();
                    customerDTO.setHeight(tempDouble);
                    System.out.print("Podaj rok urodzenia: ");
                    tempInt = new Scanner(System.in).nextInt();
                    customerDTO.setBirthYear(tempInt);
                    CustomerQuery.addCustomer(customerDTO);
                    break;
                case 3:
                    System.out.println("=========== Usun klienta: ===========");
                    System.out.print("Podaj id klienta, ktorego chcesz usunac: ");
                    String temp = new Scanner(System.in).nextLine();
                    CustomerQuery.deleteCustomer(temp);
                    break;
                case 4:
                    System.exit(1);

            }
        }
        CustomerDTO[] list = CustomerQuery.getCustomerList();
        for (CustomerDTO c : list) {
            System.out.println(c);
        }
    }

    public static void configureObjectMapper() {
        Unirest.setObjectMapper(new ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });

    }


}
