package json.person;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;

public class PersonTest {
    public static void main(String[] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        Person p = new Person("Pawel", "Jarco", LocalDate.of(1985,8,2), "242141241224");

        mapper.writeValue(new File("data/json.person.json"), p);
    }
}
