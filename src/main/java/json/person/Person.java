package json.person;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;

public class Person {
    @JsonProperty("imie")
    private String name;
    @JsonProperty("nazwisko")
    private String lastName;
    @JsonProperty("data urodzenia")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    private Date birthDate;
    @JsonIgnore
    private String pesel;

    public Person() {
    }

    public Person(String name, String lastName, LocalDate birthDate, String pesel) {
        this.name = name;
        this.lastName = lastName;
        this.birthDate = Date.from(birthDate.atStartOfDay().toInstant(ZoneOffset.UTC));
        this.pesel = pesel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", pesel='" + pesel + '\'' +
                '}';
    }
}
