package json;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class PointTest {
    public static void main(String[] args) throws IOException {
        String pointJson = "{\"x\":5, \"y\":3}";
        System.out.println(pointJson);


        ObjectMapper mapper = new ObjectMapper();
        Point p = mapper.readValue(pointJson, Point.class);

        System.out.println(p);

        Point p2 = new Point(200,300);
        String p2Json = mapper.writeValueAsString(p2);
        System.out.println(p2Json);

        Point p3 = new Point(400,500);
        mapper.writeValue(new File("point.json"), p3);

        Point p4 = mapper.readValue(new File("point.json"), Point.class);
        System.out.println(p4);
    }
}