package json.countries;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Arrays;
@JsonIgnoreProperties(ignoreUnknown = true)

public class Country {
    private int area;
    private double population;
    private String nativeName;
    private String capital;
    private String[] languages ;
    private String[] borders;
    private String alpha3Code;

    public Country() {
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public double getPopulation() {
        return population;
    }

    public void setPopulation(double population) {
        this.population = population;
    }

    public String getNativeName() {
        return nativeName;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String[] getLanguages() {
        return languages;
    }

    public void setLanguages(String[] languages) {
        this.languages = languages;
    }

    public String[] getBorders() {
        return borders;
    }

    public void setBorders(String[] borders) {
        this.borders = borders;
    }

    public String getAlpha3Code() {
        return alpha3Code;
    }

    public void setAlpha3Code(String alpha3Code) {
        this.alpha3Code = alpha3Code;
    }

    @Override
    public String toString() {
        return "Country{" +
                "area=" + area +
                ", population=" + population +
                ", nativeName='" + nativeName + '\'' +
                ", capital='" + capital + '\'' +
                ", languages=" + (languages == null ? null : Arrays.asList(languages)) +
                ", borders=" + (borders == null ? null : Arrays.asList(borders)) +
                ", alpha3Code='" + alpha3Code + '\'' +
                '}';
    }
}
