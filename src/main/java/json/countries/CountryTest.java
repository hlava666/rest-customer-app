package json.countries;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class CountryTest {
    public static void main(String[] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Country[] countries = mapper.readValue(new File("data/json.countries.json"), Country[].class);

        for(Country country:countries){
            System.out.println(country);
        }
    }
}
