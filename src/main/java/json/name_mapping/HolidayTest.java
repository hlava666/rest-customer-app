package json.name_mapping;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class HolidayTest {
    public static void main(String[] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Holiday h = mapper.readValue(new File("data/holiday.json"), Holiday.class);

        System.out.println(h);
    }
}
