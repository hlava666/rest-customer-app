package json.name_mapping;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Holiday {
    private String name;
    @JsonProperty("public")
    private boolean isPublic;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    @Override
    public String toString() {
        return "Holiday{" +
                "name='" + name + '\'' +
                ", isPublic=" + isPublic +
                '}';
    }
}
