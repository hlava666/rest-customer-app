package json.customer;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class CustomerTest {
    public static void main(String[] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        Customer c = mapper.readValue(new File("data/json.customer.json"), Customer.class);

        System.out.println(c);

        mapper.writeValue(new File("data/customer2.json"), c);
    }

}
